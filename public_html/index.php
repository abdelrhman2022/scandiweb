<?php

require "_init.php";

use App\Controllers\TaskControllers;
use App\Core\Request;
use App\Core\Router;

Router::make()
    ->get('', [TaskControllers::class, "index"])
    ->get('addproduct', [TaskControllers::class, "createProduct"])
    ->post('product/create', [TaskControllers::class, "create"])
    ->get('product/delete', [TaskControllers::class, "delete"])
    ->resolve($_POST, Request::uri(), Request::method());
