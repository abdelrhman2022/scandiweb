const selector = document.getElementById("productType");
const container = document.getElementById("container-selectedItem");
const form = document.getElementById("product_form");
const SKU = document.getElementById("SKU");
const name = document.getElementById("name");
const price = document.getElementById("price");

let value = selector.options[selector.selectedIndex].value;

form.addEventListener("submit", (e) => {
    console.log(price.value)
    if (isNaN(price.value))
        e.preventDefault();
})

selector.addEventListener("change", () => {
    const supcontainer = document.getElementById("selectedItem")
    value = selector.value;
    if (supcontainer) {
        supcontainer.remove();
    }
    switch (value) {
        case ("Book"):
            html = `
            <div id="selectedItem">
                  <h3>Please Add Weight Of Book</h3>
                <div class="preference">
                    <label for="weight">Weight</label>
                    <span class="currency-code-2">KG</span>
                    <input type="number" step="0.01" name="weight" id="weight" required>
                    
                </div>
            </div>`
            container.insertAdjacentHTML("afterbegin", html);
            break;
        case ("Furniture"):
            html = `
          
               <div id="selectedItem">
                 <h3>Please Add Space Of Furniture</h3>
                 <div class="preference">
                    <label for="dimension">height</label>
                    <span class="currency-code-2">CM</span>
                    <input type="number" step="0.01" name="height" id="height" placeholder="height in meter" required>
                    
                 </div>
                 <div class="preference">

                    <label for="name">width</label>
                    <span class="currency-code-2">CM</span>
                    <input type="number" step="0.01" name="width" id="width" placeholder="width in meter" required>
                    
                 </div>
                 <div class="preference">
                    <label for="name">Length</label>
                    <span class="currency-code-2">CM</span>
                    <input type="number" step="0.01" name="length" id="length" placeholder="Length in meter" required>
                    
                 </div>
                </div>`
            container.insertAdjacentHTML("afterbegin", html);

            break;
        case ("DVD"):
            html = `
           
            <div id="selectedItem">
             <h3>Please Add Size Of DvD</h3>
                <div class="preference">
                    <label for="size">Size</label>
                    <span class="currency-code-2">MB</span>
                    <input type="number" step="0.01" name="size" id="size" required>
                   
                </div>
                </div>`
            container.insertAdjacentHTML("afterbegin", html);
            break;
    }
})