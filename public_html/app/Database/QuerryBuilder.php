<?php

namespace App\Database;

class QueryBuilder
{
    private static $pdo;
    private static $log;

    public static function make(\PDO $pdo, $log = null)
    {
        self::$pdo = $pdo;
        self::$log = $log;
    }

    public static function get($table, $where = null)
    {
        $query = "SELECT * FROM {$table}";
        $query = self::Execute($query);
        return $query->fetchAll(\PDO::FETCH_OBJ);
    }

    public static function Execute($query, $data = [])
    {
        if (self::$log) {
            self::$log->info($query);
        }
        $statment = self::$pdo->prepare($query);
        $statment->execute(array_values($data));
        return $statment;
    }

//

    public static function insert(string $table, array $data)
    {
        $field = array_keys($data);
        $fieldStr = implode(',', $field);
        $valuesStr = str_repeat("?,", count($field) - 1) . "?";

        $query = "INSERT INTO $table ($fieldStr) VALUES ($valuesStr)";
        self::Execute($query, $data);
        redirect(home());
    }

    public static function delete($table, array $id, $column = "id", $operator = "=")
    {
        $fieldStr = "(" . implode(',', $id) . ")";
        if (!$id) {
            echo "id isn't define";
        }
        $query = "DELETE FROM $table WHERE $column $operator $fieldStr";
        self::Execute($query);
        back();
    }
}
