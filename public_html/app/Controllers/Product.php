<?php


namespace App\Controllers;

use App\Database\QueryBuilder;

class Product
{
    public static function all(string $name)
    {
        return QueryBuilder::get($name);
    }
}