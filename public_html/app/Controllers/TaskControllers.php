<?php

namespace App\Controllers;

use App\Database\QueryBuilder;

require_once('Product.php');

class TaskControllers
{
    public function index()
    {
        view("index", Product::all("products_details"));
    }

    public function createProduct()
    {
        view("create_product");
    }

    public function create(array $data)
    {
        QueryBuilder::insert("products_details", $data);
    }


    public function delete()
    {
        if (!$_GET) {
            back();
        }
        QueryBuilder::delete("products_details", array_values($_GET), "id", "IN");
    }
}
