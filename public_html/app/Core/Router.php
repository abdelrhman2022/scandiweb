<?php

namespace App\Core;

class Router
{
    private $get = [];
    private $post = [];

    public static function make()
    {
        $router = new self();
        return $router;
    }

    public function get($uri, $action)
    {
        $this->get[$uri] = $action;
        return $this;
    }

    public function post($uri, $action)
    {
        $this->post[$uri] = $action;
        return $this;
    }


    public function resolve(array $data = [], $uri, $method)
    {
        if (array_key_exists($uri, $this->{$method})) {  //
            $action = $this->{$method}[$uri];  // [[0]=>TaskControllers,[1]=>index]
            $this->callAction($data, ...$action);  // ("TaskControllers", "index")
        } else {
            redirect(home());
            // throw new Exception("Page not found");
        }
    }


    protected function callAction(array $data, $controller, $action)
    {
        $controller = new $controller();

        if (count($data) == 0) {
            $controller->{$action}();
        } else {
            $controller->{$action}($data);
        }
    }
}
