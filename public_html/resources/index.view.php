<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product List</title>
    <style>
        <?php

            require "Style/style-sheet.css"
        ?>
    </style>
</head>
<body>
<div class="container-all">

    <h1>Product list</h1>

    <hr style="margin-bottom: 2em;">
    <form action="product/delete" id="delete-form" method="GET">
        <div class="grid-container-all">

            <?php foreach ($data as $item): ?>

                <div class="grid-item">
                    <div class="check-box-container">
                        <input class="delete-checkbox" type="checkbox" name="<?php echo $item->SKU; ?>"
                               value="<?php echo $item->id; ?>">
                    </div>
                    <p><?php echo $item->SKU; ?></p>
                    <p><?php echo $item->name; ?></p>
                    <p>Price: <?php echo $item->price; ?>$</p>
                    <p>Weight: <span class="desciption-category"><?= $item->weight ?></span>KG</p>
                    <p>Dimension: <span class="desciption-category"><?= $item->height ?></span> x <span
                                class="desciption-category"><?= $item->width ?></span> <span
                                class="desciption-category"><?= $item->length ?></span> cm</p>
                    <p>Size: <span class="desciption-category"><?= $item->size ?></span> MB</p>


                </div>
            <?php endforeach ?>
            <div class="button-header-cotainer">
                <div>
                    <button id="delete-product-btn" type="submit">MASS DELETE</button>
                    <a href="<?= home() . "/addproduct"; ?>" style="    text-decoration: none;">
                        <button class="Add-btn" type="button">ADD</button>
                    </a>
                </div>
            </div>
        </div>
    </form>

</div>
<script>
    <?php require "Script/scriptFile2.js"?>
</script>
</body>
</html>