<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Add</title>
    <style>
        <?php
            require "Style/style-sheet.css";
            require "Style/style-sheet-addproduct.css";
        ?>
    </style>
</head>
<body>
<div class="container-all">

    <h1>Product Add</h1>

    <hr style="margin-bottom: 2em;">
    <form action="product/create" id="product_form" method="POST">
        <div class="form-product-cotainer">
            <div class="grid-form-container">
                <div class="preference">
                    <label for="SKU">SKU</label>
                    <input type="text" name="SKU" id="sku" placeholder="Enter The Bar Code Of Project" required>
                </div>
                <div class="preference">
                    <label for="name">name</label>
                    <input type="text" name="name" id="name" placeholder="Enter The Name Of Product" required>
                </div>
                <div class="preference">
                    <label for="price">price</label>
                    <span class="currency-code">$</span>
                    <input type="number" step="0.01" name="price" id="price" placeholder="Enter Price Of Product"
                           required>

                </div>

                <div class="preference" id="selected-preference">
                    <label for="price">Type Switcher</label>
                    <select name="productType" id="productType" required>
                        <option title="Tooltip" value="">Type Switcher</option>
                        <option value="Furniture">Furniture</option>
                        <option value="DVD">DVD</option>
                        <option value="Book">Book</option>
                    </select>
                    <div class="preference" id="container-selectedItem">
                        <div id="selectedItem"></div>
                    </div>
                </div>

            </div>
        </div>
</div>
<div class="button-header-cotainer">
    <button type="submit">Save</button>
    <a href="<?= home() ?> ">
        <button type="button" class="Add-btn">Cancel</button>
    </a>
</div>
</div>

</form>
<script>
    <?php

    require "Script/scriptFile.js";

    ?>
</script>

</body>